import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'
import Catalog from '../views/Catalog'
import Sales from '../views/Sales'
import History from '../views/History'
import Settings from '../views/Settings'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },{
    path: '/catalogo',
    name: 'Catalog',
    component: Catalog
  },{
    path: '/ventas',
    name: 'Sales',
    component: Sales
  },{
    path: '/historia',
    name: 'History',
    component: History
  },{
    path: '/ajustes',
    name: 'Settings',
    component: Settings
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
