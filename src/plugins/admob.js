import { AdPosition, AdSize } from '@capacitor-community/admob'
import { Plugins } from '@capacitor/core'

const { AdMob } = Plugins;
AdMob.initialize();


export default {
    showBanner: (adId, mediumScreen) => {
        let size = mediumScreen ? AdSize.FULL_BANNER : AdSize.BANNER,
            options = {
                adId,
                adSize: size,
                position: AdPosition.BOTTOM_CENTER,
                margin: 0,
            };
        AdMob.showBanner(options);
    }
}
