import Vue from 'vue'

import ArrowLeftIcon from 'vue-material-design-icons/ArrowLeft.vue'
Vue.component('v-arrow-left-icon', ArrowLeftIcon);
import CalendarIcon from 'vue-material-design-icons/CalendarMonth.vue'
Vue.component('v-calendar-icon', CalendarIcon);
import CancelIcon from 'vue-material-design-icons/Cancel.vue'
Vue.component('v-cancel-icon', CancelIcon);
import CashIcon from 'vue-material-design-icons/Cash.vue'
Vue.component('v-cash-icon', CashIcon);
import CircleIcon from 'vue-material-design-icons/CircleSmall.vue'
Vue.component('v-circle-icon', CircleIcon);
import CloseBoxIcon from 'vue-material-design-icons/CloseBox.vue'
Vue.component('v-close-box-icon', CloseBoxIcon);
import CogIcon from 'vue-material-design-icons/Cog.vue'
Vue.component('v-cog-icon', CogIcon);
import ContentSaveIcon from 'vue-material-design-icons/ContentSave.vue'
Vue.component('v-save-icon', ContentSaveIcon);
import DeleteIcon from 'vue-material-design-icons/Delete.vue'
Vue.component('v-delete-icon', DeleteIcon);
import FileCabinet from 'vue-material-design-icons/FileCabinet.vue'
Vue.component('v-file-cabinet-icon', FileCabinet);
import FileEdit from 'vue-material-design-icons/FileDocumentEditOutline.vue'
Vue.component('v-file-edit-icon', FileEdit);
import FoodIcon from 'vue-material-design-icons/Food.vue'
Vue.component('v-food-icon', FoodIcon);
import MinusBoxIcon from 'vue-material-design-icons/MinusBox.vue'
Vue.component('v-minus-box-icon', MinusBoxIcon);
import PlusBoxIcon from 'vue-material-design-icons/PlusBox.vue'
Vue.component('v-plus-box-icon', PlusBoxIcon);
import PencilIcon from 'vue-material-design-icons/Pencil.vue'
Vue.component('v-pencil-icon', PencilIcon);
import PlusIcon from 'vue-material-design-icons/Plus.vue'
Vue.component('v-plus-icon', PlusIcon);
import ReloadIcon from 'vue-material-design-icons/Reload.vue'
Vue.component('v-reload-icon', ReloadIcon);
import SendIcon from 'vue-material-design-icons/Send.vue'
Vue.component('v-send-icon', SendIcon);