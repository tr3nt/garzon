import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import es from 'vuetify/es5/locale/es';

Vue.use(Vuetify);

export default new Vuetify({
    lang: {
        locales: { es },
        current: 'es',
    },
    theme: {
        dark: true
    }
});

/** 
 * color schemes
 * 
    primary: '#82428E',
    secondary: '#218C7D',
    accent: '#fde7b5',
    background: '#212121'

    primary: '#1c739c',
    secondary: '#9c3b3b',
    accent: '#ffde94',
    background: '#3e3e3e'

    primary: '#ab3d89',
    secondary: '#594f80',
    accent: '#c5c9f7',
    background: '#241f2d'

    primary: '#673b0e',
    secondary: '#2b2b2b',
    accent: '#e8dea7',
    background: #9e9e9e
 */
