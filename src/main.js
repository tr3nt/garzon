import Vue from 'vue'
import App from './App.vue'
// import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import { Plugin } from 'vue-fragment'

Vue.config.productionTip = false;
Vue.use(Plugin);

/** Event Bus */
export const EventBus = new Vue();

/** General Components */
import InputMoney from './components/InputMoney'
Vue.component('v-input-money', InputMoney);
import ListProduct from './components/catalog/ListProduct'
Vue.component('v-product-list', ListProduct);

/** Material Icons */
import './plugins/icons'

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
