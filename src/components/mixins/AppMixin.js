import { mapGetters } from 'vuex';
import { EventBus } from '../../main'

export default {
    data() {
        return {
            today: new Date().toISOString().substr(0, 10),
            bus: EventBus,
            windowHeight: window.innerHeight,
            windowWidth: window.innerWidth,

            // Validation
            required: v => !!v || this.txt.req
        }
    },
    methods: {
        moneyFormat(number) {
            number = (isNaN(number) || number == null) ? 0 : parseFloat(number);
            return "$ " + number.toFixed(2).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,");
        },
        castFloat(value) {
            value = value || '0';
            let newValue = parseFloat(value.replace(/[^\d/.]/g, ""));
            return isNaN(newValue) ? parseFloat(0) : newValue;
        },
        dateFormat(date) {
            if (date !== undefined && date !== null && date.length > 0) {
                let d = date.split('-'),
                    m = this.txt.jan;
                switch(d[1]) {
                    case '02': m = this.txt.feb; break;
                    case '03': m = this.txt.mar; break;
                    case '04': m = this.txt.apr; break;
                    case '05': m = this.txt.may; break;
                    case '06': m = this.txt.jun; break;
                    case '07': m = this.txt.jul; break;
                    case '08': m = this.txt.aug; break;
                    case '09': m = this.txt.sep; break;
                    case '10': m = this.txt.oct; break;
                    case '11': m = this.txt.nov; break;
                    case '12': m = this.txt.dec;
                }
                if (this.txt.jan == 'January')
                    return `${m} ${d[2]}, ${d[0]}`;
                else
                    return `${d[2]} de ${m} de ${d[0]}`;
            }
        },
        deepCopy(val) {
            return JSON.parse(JSON.stringify(val))
        },
        bigScreen() {
            return this.$vuetify.breakpoint.name == 'md'
                || this.$vuetify.breakpoint.name == 'lg'
                || this.$vuetify.breakpoint.name == 'xl'
        },
        mediumScreen() {
            return this.$vuetify.breakpoint.name == 'sm'
                || this.$vuetify.breakpoint.name == 'md'
                || this.$vuetify.breakpoint.name == 'lg'
                || this.$vuetify.breakpoint.name == 'xl'
        },

        /** Settings */
        setColorScheme(color) {
            this.$vuetify.theme.themes.dark.primary = color.primary;
            this.$vuetify.theme.themes.dark.secondary = color.secondary;
            this.$vuetify.theme.themes.dark.accent = color.accent;
            this.$vuetify.theme.themes.dark.background = color.background;
        },
        onResize() {
            this.windowHeight = window.innerHeight
            this.windowWidth = window.innerWidth
        }
    },
    computed: {
        ...mapGetters(['txt', 'color'])
    },
    mounted() {
        this.bus.$on('color-scheme', this.setColorScheme);
        this.$nextTick(() => {
            window.addEventListener('resize', this.onResize);
        })
    },
    beforeDestroy() {
        window.removeEventListener('resize', this.onResize)
    }
}