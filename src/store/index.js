import Vue from 'vue'
import Vuex from 'vuex'
import system from './system'
import products from './products'
import sales from './sales'
import statistics from './statistics'
import { EventBus } from '../main'

/** Local Storage */
import { Plugins } from '@capacitor/core'
const { Storage } = Plugins;

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    sideBarActive: false
  },
  getters: {
    sideBar: state => {
      return state.sideBarActive
    }
  },
  mutations: {
    setSideBar: (state, active) => {
      state.sideBarActive = active
    }
  },
  actions: {
    SAVE: async ({ dispatch }, params) => {
      let [key, data, message] = params;
      await Storage.set({
        key,
        value: JSON.stringify(data)
      });
      if (message)
        dispatch('setToast', message);
      dispatch('EMIT', { title: `${key}-save` });
    },
    GET: async ({ commit }, params) => {
      let [ key, mutation ] = params,
        response = await Storage.get({ key });
      commit(mutation, JSON.parse(response.value));
    },
    CLEAR: async () => {
      await Storage.clear()
    },
    EMIT: (context, {title, params}) => {
      let par = typeof params == 'object' ? params : true;
      EventBus.$emit(title, par);
    },
    toggleSideBar: ({ commit }, active) => {
      commit('setSideBar', active)
    }
  },
  modules: {
    system,
    products,
    sales,
    statistics
  }
})
