/* eslint-disable no-unused-vars */
export const CleanSale = state => {
    state.sale = {
        id: 0,
        date: new Date().toISOString().substr(0, 10),
        name: '',
        products: [],
        tip: parseFloat(0),
        total: parseFloat(0),
        received: parseFloat(0),
        change: parseFloat(0)
    }
}

export const GroupBy = (list, keyGetter) => {
    let map = new Map(),
        listSales = list || [],
        key, collection;

    listSales.forEach(item => {
        key = keyGetter(item);
        collection = map.get(key);
        if (!collection) {
            map.set(key, [item]);
        } else {
            collection.push(item);
        }
    });
    return map;
}

export const CastFloat = value => {
    value = value || '0';
    let newValue = parseFloat(value.replace(/[^\d/.]/g, ""));
    return isNaN(newValue) ? parseFloat(0) : newValue;
}
/* eslint-enable no-unused-vars */