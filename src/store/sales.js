import { CleanSale, CastFloat } from './functions'

export default {
    state: {
        sale: {
            id: 0,
            date: new Date().toISOString().substr(0, 10),
            name: '',
            products: [],
            tip: parseFloat(0),
            total: parseFloat(0),
            received: parseFloat(0),
            change: parseFloat(0)
        },
        salesPending: [],
        salesFinished: []
    },
    getters: {
        sale: state => {
            return state.sale
        },
        salesPending: state => {
            return state.salesPending
        },
        salesFinished: state => {
            return state.salesFinished
        }
    },
    mutations: {
        setSale: (state, sale) => {
            state.sale.id = sale.id;
            state.sale.date = typeof sale.date == 'string' ? sale.date : new Date().toISOString().substr(0, 10);
            state.sale.name = sale.name;
            state.sale.products = sale.products;
            state.sale.tip = parseFloat(0);
            state.sale.total = sale.total;
            state.sale.received = parseFloat(0);
            state.sale.change = parseFloat(0);
        },
        setSaleDate: (state, date) => {
            state.sale.date = date
        },
        setSaleName: (state, name) => {
            state.sale.name = name
        },
        addSaleProducts: (state, product) => {
            let find = element => element.name == product.name,
                productsCopy = JSON.parse(JSON.stringify(state.sale.products)),
                index = productsCopy.findIndex(find),
                total = 0;

            if (index >= 0)
                ++productsCopy[index].quantity
            else
                product.quantity = 1,
                productsCopy.push(product);

            productsCopy.map(product => {
                total += product.price * product.quantity;
            });
            state.sale.total = total;
            state.sale.products = productsCopy;
        },
        removeSaleProducts: (state, name) => {
            let find = element => element.name == name,
                productsCopy = JSON.parse(JSON.stringify(state.sale.products)),
                index = state.sale.products.findIndex(find);

            productsCopy.splice(index, 1);
            state.sale.products = productsCopy;
        },
        updateSaleNumProducts: (state, params) => {
            let [name, action] = params, total = 0,
                find = element => element.name == name,
                productsCopy = JSON.parse(JSON.stringify(state.sale.products)),
                index = state.sale.products.findIndex(find);

            if (action == 'add')
                productsCopy[index].quantity += 1;
            else if (state.sale.products[index].quantity > 1)
                productsCopy[index].quantity -= 1;
            else
                productsCopy.splice(index, 1);

            productsCopy.map(product => {
                total += product.price * product.quantity;
            });
            state.sale.total = total;
            state.sale.products = productsCopy;
        },
        setSaleTip: (state, tip) => {
            state.sale.tip = CastFloat(tip);
            let val = state.sale.received - (state.sale.total + state.sale.tip);
            state.sale.change = val > 0 ? val : parseFloat(0);
        },
        setSaleTotal: (state, total) => {
            state.sale.total = parseFloat(total)
        },
        setSaleReceived: (state, received) => {
            state.sale.received = CastFloat(received);
            let val = state.sale.received - (state.sale.total + state.sale.tip);
            state.sale.change = val > 0 ? val : parseFloat(0);
        },
        setSalesPending: (state, pending) => {
            state.salesPending = pending || []
        },
        addSalesPending: state => {
            let pendingCopy = JSON.parse(JSON.stringify(state.salesPending)),
                id = 1;

            pendingCopy.push({ ...state.sale });
            pendingCopy.map(sale => {
                sale.id = id;
                ++id;
            });
            state.salesPending = pendingCopy;

            CleanSale(state);
        },
        updateSalesPending: state => {
            let find = element => element.id == state.sale.id,
                index = state.salesPending.findIndex(find);

            state.salesPending[index] = { ...state.sale };

            CleanSale(state);
        },
        setSalesFinished: (state, finished) => {
            state.salesFinished = finished || []
        },
        movePendingToFinished: state => {

            let find = element => element.name == state.sale.name,
                index = state.salesPending.findIndex(find),
                pendingCopy = JSON.parse(JSON.stringify(state.salesPending)),
                finishedCopy = JSON.parse(JSON.stringify(state.salesFinished)),
                finishedSale = {
                    dat: state.sale.date,
                    nam: state.sale.name,
                    tip: state.sale.tip,
                    tot: state.sale.total
                };

            finishedCopy.push(finishedSale);
            state.salesFinished = finishedCopy;

            pendingCopy.splice(index, 1);
            state.salesPending = pendingCopy;

            CleanSale(state);
        }
    },
    actions: {
        getPending: ({ dispatch }) => {
            dispatch('GET', ['pending', 'setSalesPending'])
        },
        savePending: ({ state, dispatch }, message) => {
            dispatch('SAVE', ['pending', state.salesPending, message])
        },
        getFinished: ({ dispatch }) => {
            dispatch('GET', ['finished', 'setSalesFinished'])
        },
        saveFinished: ({ state, dispatch }, message) => {
            dispatch('SAVE', ['finished', state.salesFinished, message])
        },
        addToPending: ({ rootState, commit, dispatch }) => {
            commit('addSalesPending')
            dispatch('savePending', rootState.system.txt.orderSav)
        },
        updatePending: ({ rootState, commit, dispatch }) => {
            commit('updateSalesPending')
            dispatch('savePending', rootState.system.txt.orderUpd2)
        },
        moveToFinished: ({ rootState, commit, dispatch }) => {
            commit('movePendingToFinished')
            dispatch('savePending', false)
            dispatch('saveFinished', rootState.system.txt.orderUpd)
        },
        cleanSale: ({ state }) => {
            CleanSale(state)
        }
    }
}