import { GroupBy } from './functions'
import { Plugins } from '@capacitor/core'

const { Storage } = Plugins;

export default {
    state: {
        listFinished: [],
        totals: {}
    },
    getters: {
        listSalesFinished: state => {
            return state.listFinished
        },
        totalsSalesFinished: state => {
            return state.totals
        }
    },
    mutations: {
        setListSalesFinished: (state, listFinished) => {
            state.listFinished = listFinished
        },
        setTotalsSalesFinished: (state, totals) => {
            state.totals = totals
        }
    },
    actions: {
        getSalesFinishedByDate: async ({ commit }, date) => {
            let response = await Storage.get({ key: 'finished' }),
                parsed = JSON.parse(response.value),
                finished = GroupBy(parsed, sale => sale.dat),
                finishedByDate = finished.get(date) || [],
                id = 1, total = 0, tip = 0, totalTip = 0,
                customers = finishedByDate.length;

            finishedByDate.map(sale => {
                totalTip += sale.tot + sale.tip;
                total += sale.tot;
                tip += sale.tip;
                sale.id = id;
                ++id;
            });
            commit('setTotalsSalesFinished', { total, tip, totalTip, customers });
            commit('setListSalesFinished', finishedByDate);
        }
    }
}