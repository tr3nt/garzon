import { Plugins } from '@capacitor/core'
import { EventBus } from '../main'
import ES from './es'
import EN from './en'

const { Modals, Toast, Device, Storage } = Plugins

export default {
    state: {
        txt: {},
        settings: {
            lang: '',
            colorScheme: {
                id: 4
            }
        },
        newSettings: {
            lang: 'en',
            colorScheme: {
                id: 4,
                primary: '#673b0e',
                secondary: '#2b2b2b',
                accent: '#f9e7cd',
                background: '#ffeeee'
            },
            device: {}
        }
    },
    getters: {
        txt: state => {
            return state.txt
        },
        settings: state => {
            return state.settings
        },
        color: state => {
            return state.settings.colorScheme.id
        }
    },
    mutations: {
        setTxt: (state, lang) => {
            state.settings.lang = lang
            if (state.settings.lang == 'es')
                state.txt = ES
            else
                state.txt = EN
        },
        setSettings: (state, settings) => {
            state.settings = settings
        },
        setColorScheme: (state, colorScheme) => {
            state.settings.colorScheme = colorScheme
        }
    },
    actions: {
        setAlert: async (context, message) => {
            await Modals.alert({
                title: 'KELLNER',
                message
            })
        },
        setPrompt: async ({ commit }, params) => {
            let [question, value, mutation] = params,
                response = await Modals.prompt({
                    title: 'KELLNER',
                    message: question,
                    inputText: value.toString()
                })
            if (!response.cancelled)
                commit(mutation, response.value)
        },
        setPrompt2: async (context, params) => {
            let [question, value, event] = params,
                response = await Modals.prompt({
                    title: 'KELLNER',
                    message: question,
                    inputText: value.toString()
                })
            if (!response.cancelled)
                EventBus.$emit(event, response.value)
        },
        setConfirm: async ({ dispatch }, data) => {
            let [message, action, params] = data,
                confirm = await Modals.confirm({
                    title: 'CONFIRMAR',
                    message
                });
            if (confirm.value)
                dispatch(action, params)
        },
        setToast: async (context, text) => {
            await Toast.show({
                text
            })
        },
        getSettings: async ({ state, commit, dispatch }) => {
            let response = await Storage.get({ key: 'settings' }),
                settings = JSON.parse(response.value);
            if (settings !== null) {
                commit('setSettings', settings)
                commit('setTxt', settings.lang)
            }
            else {
                let lang = await Device.getLanguageCode(),
                    locale = lang.value;
                state.newSettings.lang = locale == 'es' ? 'es' : 'en';
                state.newSettings.device = await Device.getInfo();
                commit('setSettings', state.newSettings);
                commit('setTxt', locale);
                dispatch('saveSettings', state.txt.successSett);
            }
            EventBus.$emit('color-scheme', state.settings.colorScheme)
        },
        changeColorScheme: ({ state, commit, dispatch }, id) => {
            let colorScheme = { id, primary: '#34a7a7', secondary: '#1f5049', accent: '#2f2f2f', background: '#383838' };
            switch (id) {
                case 2:
                    colorScheme = { id, primary: '#286d8e', secondary: '#9c3b3b', accent: '#ffcaca', background: '#f0dfdf' }; break;
                case 3:
                    colorScheme = { id, primary: '#ab3d89', secondary: '#594f80', accent: '#cbcbf0', background: '#dfdff0' }; break;
                case 4:
                    colorScheme = { id, primary: '#673b0e', secondary: '#2b2b2b', accent: '#e8e1d5', background: '#ffeeee' };
            }
            commit('setColorScheme', colorScheme)
            EventBus.$emit('color-scheme', colorScheme);
            dispatch('saveSettings', state.txt.successColor);
        },
        changeLanguage: ({ state, commit, dispatch }, lang) => {
            commit('setTxt', lang);
            dispatch('saveSettings', state.txt.successLang);
        },
        saveSettings: ({ state, dispatch }, message) => {
            dispatch('SAVE', ['settings', state.settings, message])
        },
        removeAllData: async (context, accept) => {
            if (accept)
                await Storage.clear()
        }
    }
}