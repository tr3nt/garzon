export default {
    state: {
        catProductList: [
            'Comidas',
            'Bebidas',
            'Paquetes',
            'Promociones'
        ],
        productList: []
    },
    getters: {
        catalogList: state => {
            return state.catProductList
        },
        productList: state => {
            return state.productList
        }
    },
    mutations: {
        setProductList: (state, productList) => {
            state.productList = productList || []
        }
    },
    actions: {
        getProductList: ({ dispatch }, type) => {
            dispatch('GET', [type, 'setProductList'])
        },

        addToProductList: ({ state, dispatch }, params) => {
            let [type, product, message] = params;
            state.productList.push(product);
            dispatch('SAVE', [type, state.productList, message]);
        },

        updateProductList: ({ state, dispatch }, params) => {
            let [type, product, oldName, message] = params,
                find = element => element.name == oldName,
                index = state.productList.findIndex(find);
            state.productList[index] = product;
            dispatch('SAVE', [type, state.productList, message]);
            dispatch('EMIT', { title: 'product-update' });
        },

        removeProductList: ({ state, dispatch }, params) => {
            let [type, name, message] = params,
                find = element => element.name == name,
                itemIndex = state.productList.findIndex(find);
            state.productList.splice(itemIndex, 1);
            dispatch('SAVE', [type, state.productList, message]);
        }
    }
}